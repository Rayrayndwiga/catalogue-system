/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package catalogue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *This class is used to model a catalogue object.
 * The object has a name which is its unique identifier.
 * The class is serializable so that it can be saved to a serialized file.
 * @author briankaranindwiga
 */
public class Catalogue implements Serializable {

    private List<Multimedia> entryList; //List of entries for the catalogue;
    private String catName; //name of the catalogue.

    /**
     * This is the constructor of the class.
     *
     * @param _id the value of the ID of the catalogue.
     */
    public Catalogue(String name) {
        SetName(name);
        entryList = new ArrayList<>();
    }

    /**
     * This is used to get the name of the catalogue.
     *
     * @return String.
     */
    public String GetName() {
        return catName;
    }

    /**
     * This is used to set the name value of the catalogue.
     *
     * @param _name the new value of the name of the catalogue.
     */
    public void SetName(String _name) {
        catName = _name;
    }

    /**
     * This is used to get the list of entries in the catalogue.
     *
     * @return List<Multimedia>.
     */
    public List<Multimedia> GetCatalogue() {
        return entryList;
    }

    /**
     * This is used to set the value of the list of entries of the catalogue.
     *
     * @param _list the new value of the list of entries of the catalogue.
     */
    public void SetCatalogue(List<Multimedia> _list) {
        entryList = _list;
    }

    public boolean equals(Catalogue otherCat) {
        if (catName.equals(otherCat.GetName())) {
            return true;
        }

        return false;
    }
}
