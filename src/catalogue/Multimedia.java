/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package catalogue;

import java.io.Serializable;
import java.util.Date;

/**
 *This class creates a multimedia object that has various attributes such as
 * a name, an ID , a date of creation, an image file path and a text file path.
 * @author briankaranindwiga
 */
public class Multimedia implements Serializable {
    /**
     * name of the multimedia.
     */
    private String name;
    /**
     * id of the multimedia.
     */
    private int id;
    /**
     * date of creation of the multimedia.
     */
    private Date doc;
    /**
     * text description of the multimedia.
     */
    private String text;
    /**
     * image of the multimedia.
     */
    private String image;

    /**
     * This is the constructor of the object.
     *
     * @param _name value of the name of the object.
     * @param _id value of the id of the object.
     * @param _doc value of the date of creation of the object.
     * @param _text value of the text of the object.
     * @param _image value of the image of the object.
     */
    public Multimedia(String _name, int _id, Date _doc, String _text, String _image) {
        SetMultimedia(_name, _id, _doc, _text, _image);
    }

    /**
     * This is used to set the properties of the object.
     *
     * @param _name value of the name of the object.
     * @param _id value of the id of the object.
     * @param _doc value of the date of creation of the object.
     * @param _text value of the text of the object.
     * @param _image value of the image of the object.
     */
    public void SetMultimedia(String _name, int _id, Date _doc, String _text, String _image) {
        SetName(_name);
        SetId(_id);
        SetDate(_doc);
        SetText(_text);
        SetImage(_image);
    }

    /**
     * This method is used to get the name of the object.
     *
     * @return String.
     */
    public String GetName() {
        return name;
    }

    /**
     * This method is used to set the value of the name of the object.
     *
     * @param _name the new value of the name of the object.
     */
    public void SetName(String _name) {
        name = _name;
    }

    /**
     * This method is used to get the id of the object.
     *
     * @return int.
     */
    public int GetId() {
        return id;
    }

    /**
     * This method is used to set the value of the id of the object.
     *
     * @param _id the new value of the id of the object.
     */
    public void SetId(int _id) {
        id = _id;
    }

    /**
     * This method is used to get the date of creation of the object.
     *
     * @return Date.
     */
    public Date GetDate() {
        return doc;
    }

    /**
     * This method is used to set the value of the date of creation of the
     * object.
     *
     * @param _date the new value of the date of creation of the object.
     */
    public void SetDate(Date _doc) {
        doc = _doc;
    }

    /**
     * This method is used to get the text description of the object.
     *
     * @return String.
     */
    public String GetText() {
        return text;
    }

    /**
     * This method is used to set the value of the text of the object.
     *
     * @param _text the new value of the text of the object.
     */
    public void SetText(String _text) {
        text = _text;
    }

    /**
     * This method is used to get the image of the object.
     *
     * @return BufferedImage.
     */
    public String GetImage() {
        return image;
    }

    /**
     * This method is used to set the value of the image of the object.
     *
     * @param _image the new value of the image of the object.
     */
    public void SetImage(String _image) {
        image = _image;
    }
}
