/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package catalogue;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 * This class is mainly associated with the operations of the catalogues. It is
 * mainly used by the GUI so as to undertake most of the catalogue operations.
 *
 * @author briankaranindwiga
 */
public class CatalogueClient {

    /**
     * List of catalogues
     */
    private List<Catalogue> catalogueList;

    /**
     * This is the main constructor for the CatalogueClient class.
     */
    public CatalogueClient() {
        catalogueList = new ArrayList<>();
    }

    /**
     * This method is used to get the catalogue list.
     *
     * @return List<Catalogue>.
     */
    public List<Catalogue> GetCatalogueList() {
        return catalogueList;
    }

    /**
     * This method is used to check if a string is empty or has empty characters
     *
     * @param input - the string to be checked
     * @return boolean
     */
    public boolean CheckString(String input) {
        String checkedinput = "";
        input = input.trim();
        if (input.length() == 0 && input.equals("")) {
            return false;
        }
        return true;
    }

    /**
     * This method is used to add a catalogue to the list of catalogues.
     *
     * @param _cat name of the catalogue to be added to the list.
     */
    public void AddCatalogue(String catName) {

        catalogueList.add(new Catalogue(catName));
    }

    /**
     * This method is used to remove a catalogue from the list of catalogues.
     * This method checks the id of the catalogue passed into the method and
     * checks through the list of catalogues and removes the catalogue from the
     * list.
     *
     * @param _catName name of the catalogue to be removed.
     * @return boolean.
     */
    public boolean RemoveCatalogue(String _catName) {
        for (Catalogue _cat : catalogueList) {
            if (_cat.GetName().equals(_catName)) {
                catalogueList.remove(_cat);
                return true;
            }
        }
        return false;
    }

    /**
     * This method is used to create a all catalogue folders if they do not
     * already exist.
     */
    public void CreateCatalogueFolders() {
        try {
            //Creating the main folder for all catalogues
            String mainFolder = "Catalogues";
            File theMainDir = new File(mainFolder);
            // if the directory does not exist, create it
            if (!theMainDir.exists()) {
                boolean result = theMainDir.mkdir();
            }

            //Creating folder for the images
            String catImages = "Images";
            File imageDir = new File(mainFolder + "/" + catImages);

            // if the directory does not exist, create it
            if (!imageDir.exists()) {
                boolean result = imageDir.mkdir();
            }

            //Creating folder for the texts
            String catTexts = "Texts";
            File textsDir = new File(mainFolder + "/" + catTexts);
            // if the file does not exist, create it
            if (!textsDir.exists()) {
                boolean result = textsDir.mkdir();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * This method is used to check if a catalogue exists with the name that is
     * passed into the method.
     *
     * @param _name name to be checked.
     * @return boolean.
     */
    public boolean isCatalogue(String _name) {
        for (Catalogue _cat : catalogueList) {
            if (_cat.GetName().equals(_name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method is used to load up all the previously saved catalogues into
     * the program.
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void loadCatalogues() throws IOException, ClassNotFoundException {
        File catDir = new File("Catalogues");

        String[] cats = catDir.list(new FilenameFilter() { //read all files in directory ending with .ser
            @Override
            public boolean accept(File dir, String string) {
                return string.endsWith(".ser");
            }
        });
        //add serialized files to list of catalogues
        for (String _catname : cats) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Catalogues/" + _catname))) {
                Catalogue _cat = (Catalogue) ois.readObject();
                catalogueList.add(_cat);
            }
        }
    }

    /**
     * This method is used to save catalogues to a serializable file.
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void saveCatalogues() throws FileNotFoundException, IOException {
        for (File f : new File("Catalogues").listFiles()) {//Delete all files from directory
            if (!f.isDirectory()) {
                f.delete();
            }
        }
        for (Catalogue _cat : catalogueList) {
            try (ObjectOutputStream outstream = new ObjectOutputStream(new FileOutputStream(new File("Catalogues/" + _cat.GetName() + ".ser")))) {
                outstream.writeObject(_cat);
                outstream.flush();
            }
        }

    }

    /**
     * This method is used to get the index of a catalogue in the list from its
     * name.
     *
     * @param _name name of the catalogue's index to be retrieved.
     * @return int.
     */
    public int GetCatIndex(String _name) {
        int index = 0;
        int i = 0;
        for (Catalogue _cat : catalogueList) {
            if (_cat.GetName().equals(_name)) {
                index = i;
            }
            i++;
        }
        return index;
    }

    /**
     * This method is used to get all the names for all saved catalogues.
     *
     * @return String[] of names of catalogues.
     */
    public String[][] GetCatNames() {
        String[][] names = new String[catalogueList.size()][1];

        for (int i = 0; i < names.length; i++) {
            names[i][0] = catalogueList.get(i).GetName();
        }

        return names;
    }

    /**
     * This method is used to get all the names of the entries in a particular
     * catalogue.
     *
     * @param _name name of the catalgoue the entries are being gotten.
     * @return String[][] of the entries in a particular catalogue.
     */
    public String[][] GetCatEntryNames(String _name) {

        String[][] entries = new String[catalogueList.get(GetCatIndex(_name)).GetCatalogue().size()][2];

        for (int i = 0; i < entries.length; i++) {
            entries[i][0] = catalogueList.get(GetCatIndex(_name)).GetCatalogue().get(i).GetName();
            entries[i][1] = Integer.toString(catalogueList.get(GetCatIndex(_name)).GetCatalogue().get(i).GetId());
        }
        return entries;
    }

    /**
     * This method is used to get the date of creation of a multimedia object
     * from the system.
     *
     * @return Date.
     */
    public Date GetCreationDate() {
        return Calendar.getInstance().getTime();
    }

    /**
     * This method is used to set the id of a multimedia in a catalogue.
     *
     * @param catName name of the catalogue in which a multimedia is being set
     * for an id.
     * @return id of multimedia.
     */
    public int catMultimediaGetId(String catName) {
        int id = 0;
        int catIndex = GetCatIndex(catName);

        if (catalogueList.get(catIndex).GetCatalogue().isEmpty()) {
            
            id = 1;
        } else {
            id = catalogueList.get(catIndex).GetCatalogue().size() + 1;
        }
        return id;
    }

    /**
     * This method is used to check if an id being passed is for a multimedia in
     * a specific catalogue.
     *
     * @param catIndex index of the catalogue which a multimedia is being
     * checked if is available.
     * @param newMultId id to be checked.
     * @return boolean.
     */
    public boolean isCatMultimedia(int catIndex, int newMultId) {
        for (Multimedia _mult : catalogueList.get(catIndex).GetCatalogue()) {
            if (_mult.GetId() == newMultId) {
                return true;
            }
        }

        return false;
    }

    /**
     * This method is used to add a new multimedia to a catalogue.
     *
     * @param catIndex index of the catalogue which a multimedia is being added
     * to.
     * @param _name name of the multimedia.
     * @param _id ID of the multimedia.
     * @param _doc date of creation of the multimedia.
     * @param _text path of the text file of the multimedia.
     * @param _image path of the image file of the multimedia.
     * @return
     */
    public boolean addMultimedia(int catIndex, String _name, int _id, Date _doc, String _text, String _image) {

        if (!isCatMultimedia(catIndex, _id)) {
            if (catalogueList.get(catIndex).GetCatalogue().add(new Multimedia(_name, _id, _doc, _text, _image))) {
                return true;
            }
        }

        return false;
    }

    /**
     * This method is used to delete a multimedia from a catalogue.
     *
     * @param catIndex index of the catalogue where the multimedia is being
     * removed.
     * @param multID index of the multimedia being removed.
     * @return boolean.
     */
    public boolean deleteMultimedia(int catIndex, int multID) {
        if (isCatMultimedia(catIndex, multID)) {
            for (Multimedia _mult : catalogueList.get(catIndex).GetCatalogue()) {
                if (_mult.GetId() == multID) {
                    resetCatMultimediaID(catIndex, multID);
                    catalogueList.get(catIndex).GetCatalogue().remove(_mult);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * This method is used to update the details of a multimedia.
     *
     * @param catIndex index of the catalogue where the multimedia is being
     * edited.
     * @param multID index of the multimedia being edited.
     * @param name name of the multimedia.
     * @param textpath path of the text file of the multimedia.
     * @param imagepath path of the image file of the multimedia.
     * @return boolean.
     */
    public boolean updateMultInfo(int catIndex, int multID, String name, String textpath, String imagepath) {
        for (Multimedia _mult : catalogueList.get(catIndex).GetCatalogue()) {
            if (_mult.GetId() == multID) {
                _mult.SetName(name);
                if (!textpath.equals("")) {
                    _mult.SetText(textpath);
                }
                if (!imagepath.equals("")) {
                    _mult.SetImage(imagepath);
                }
                return true;
            }
        }

        return false;
    }

    /**
     * This method is used to reset the ID's of multimedias inside a catalogue
     * incase a multimedia is removed at any one position.
     *
     * @param catIndex index of the catalogue where the multimedia is being
     * removed.
     * @param multID index of the catalogue being removed.
     */
    public void resetCatMultimediaID(int catIndex, int multID) {
        int k = 0;
        for (int i = 0; i < catalogueList.get(catIndex).GetCatalogue().size(); i++) {
            if (catalogueList.get(catIndex).GetCatalogue().get(i).GetId() == multID) {
                k = i;
                break;
            }
        }

        for (int l = (k + 1); l < catalogueList.get(catIndex).GetCatalogue().size(); l++) {
            catalogueList.get(catIndex).GetCatalogue().get(l).SetId(catalogueList.get(catIndex).GetCatalogue().get(l).GetId() - 1);
        }
    }

    /**
     * This method is used to get the index of a multimedia in a specific
     * catalogue.
     *
     * @param catIndex the index of the catalogue in which the multimedia is
     * being searched for.
     * @param multID the ID of the multimedia that is being searched for.
     * @return index of multimedia.
     */
    public int GetMultIndex(int catIndex, int multID) {
        int index = 0;
        int i = 0;
        for (Multimedia _mult : catalogueList.get(catIndex).GetCatalogue()) {
            if (_mult.GetId() == multID) {
                index = i;
            }
            i++;
        }
        return index;
    }

    /**
     * This method is used to check if a multimedia file exists or is
     * empty.
     *
     * @param fileName text path of the file to be checked.
     * @return boolean.
     */
    public boolean CheckMultFile(String filePath) {
        File file = new File(filePath);
        if (file.exists() && file.length() > 0) {
            return true;
        }
        return false;
    }

    /**
     * This method is used to get text from file and set a text area with the
     * text from the file.
     *
     * @param filePath path of the text file.
     * @return a JText area with the set text.
     */
    public JTextArea GetMultTextFromFile(String filePath) {
        JTextArea area = new JTextArea();
        try {
            FileReader fr = new FileReader(filePath);
            BufferedReader br = new BufferedReader(fr);
            area.read(br, null);
            br.close();
            fr.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return area;

        }
    }

    /**
     * This method is used to get the image for a particular multimedia and set
     * the size of the image.
     *
     * @param filePath path of the image file;
     * @return ImageIcon
     */
    public ImageIcon GetMultImage(JLabel label, String filePath) {
        ImageIcon icon = new ImageIcon(filePath);
        Image image = icon.getImage();
        BufferedImage buffImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics g = buffImage.createGraphics();
        g.drawImage(image, 0, 0, label.getWidth(), label.getHeight(), null);

        return new ImageIcon(buffImage);
    }

    /**
     * This method is used to copy a file from another destination of a computer
     * if it is not presently found in a particular folder.
     *
     * @param sourcePath the source path of the file chosen in a file chooser.
     * @param fileType the type of the file either 1 for text files and 2 for
     * image files.
     * @return path of the file;
     * @throws IOException
     */
    public String CopyFile(String sourcePath, int fileType) throws IOException {
        File sourceFile = new File(sourcePath);
        String pathName = "";

        if (fileType == 1) {//for text files
            File txtDir = new File("Catalogues/Texts");
            if (txtDir.list().length == 0) {
                Files.copy(sourceFile.toPath(), new File(txtDir + "/" + sourceFile.getName()).toPath());
                pathName = txtDir + "/" + sourceFile.getName();
                
            } else {
                boolean exists = false;

                for (String name : txtDir.list()) {
                    if (sourceFile.getName().equals(name)) {
                        exists = true;
                        break;
                    }
                }
                if (exists) {
                    pathName = txtDir + "/" + sourceFile.getName();
                } else {
                    Files.copy(sourceFile.toPath(), new File(txtDir + "/" + sourceFile.getName()).toPath());
                    pathName = txtDir + "/" + sourceFile.getName();
                }
            }

        } else if (fileType == 2) {//for image files
            File imgDir = new File("Catalogues/Images");
            if (imgDir.list().length == 0) {
                Files.copy(sourceFile.toPath(), new File(imgDir + "/" + sourceFile.getName()).toPath());
                pathName = imgDir + "/" + sourceFile.getName();
            } else {
                boolean exists = false;

                for (String name : imgDir.list()) {
                    if (sourceFile.getName().equals(name)) {
                        exists = true;
                        break;
                    }
                }

                if (exists) {
                    pathName = imgDir + "/" + sourceFile.getName();
                } else {
                    Files.copy(sourceFile.toPath(), new File(imgDir + "/" + sourceFile.getName()).toPath());
                    pathName = imgDir + "/" + sourceFile.getName();
                }
            }

        }
        return pathName;
    }
    
    /**
     * This method is used to check if a path is for a text file.
     * @param pathName name of the file to be checked.
     * @return Boolean.
     */
    public boolean isTextFile(String pathName){
        if(pathName.contains(".txt"))
            return true;
        
        return false;
    }
    
    /**
     * This method is used to check if a path is for a jpg image file.
     * @param pathName name of the file to be checked.
     * @return Boolean.
     */
    public boolean isImageFile(String pathName){
        if(pathName.contains(".jpg")||pathName.contains(".jpeg")||pathName.contains(".JPG")||pathName.contains(".JPEG"))
            return true;
        
        return false;
    }
}