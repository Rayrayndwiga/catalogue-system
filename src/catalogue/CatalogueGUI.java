/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package catalogue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;


/**
 *This class is used to provide a GUI that is used to perform the operation of the catalogue system.
 * The GUI does all the validation for the user's input.
 * @author Brian Ndwiga
 */
public class CatalogueGUI extends JFrame {
    /**
     * The main frame of the GUI.
     */
    private JFrame pane = (JFrame)SwingUtilities.getRoot(this);
    /**
     * The catalogueCLient class that does the operations of the catalogues.
     */
    private CatalogueClient catClient;
    /**
     * The menu bar.
     */
    private JMenuBar menubar; 
    /**
     * File menu option.
     */
    private JMenu file; 
    /**
     * Menu items.
     */
    private JMenuItem menuitem;
    /**
     * labels for the main label and save label.
     */
    private JLabel label,saveLabel; 
    /**
     * panels for the main panel and saving panel
     */
    private JPanel mainpanel,savePanel;
    /**
     * The colour scheme for the panels.H-Hue, S- Saturation, L-
     */
    float H, S, L;//colour for the panels
    /**
     * Colour scheme for the buttons.
     */
    private float bH,bS,bL;//colour for the button text
    
   
    /**
     * The public constructor of the CatalogueGUI
     * @param frameTitle the title of the GUI.
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public CatalogueGUI(String frameTitle) throws IOException, ClassNotFoundException {
        super(frameTitle);
        //
        bH = new Float(0.50); bS = new Float(0.45); bL = new Float(0.42);
        H = new Float(0.56); S = new Float(0.14); L = new Float(0.77);
        //
        catClient = new CatalogueClient();
        catClient.CreateCatalogueFolders();
        catClient.loadCatalogues();
        //
        mainpanel = new JPanel();
        mainpanel.setBackground(Color.getHSBColor(H, S, L));
        label = new JLabel("WELCOME TO THE CATALOGUE MANAGER", JLabel.CENTER);
        label.setFont(new Font("Century Gothic",Font.BOLD, 20));
        label.setForeground(Color.getHSBColor(bH, bS, bL));
        new HomePanel();
        
        menubar = new JMenuBar();
        getContentPane().add(mainpanel, BorderLayout.CENTER);

        //First menu option
        file = new JMenu("File");
        file.setSize(100, 50);
        menubar.add(file);
        getContentPane().add(menubar, BorderLayout.PAGE_START);

        //Menu Items for File menu
        //First Item
        menuitem = new JMenuItem("Home");
        menuitem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                new HomePanel();
            }
        });
        file.add(menuitem);
        
        //Second Item
        menuitem = new JMenuItem("Save");
        //save panel 
        savePanel = new JPanel();
        savePanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
        getContentPane().add(savePanel, BorderLayout.SOUTH);
        savePanel.setPreferredSize(new Dimension(getContentPane().getWidth(), 16));
        savePanel.setLayout(new BoxLayout(savePanel, BoxLayout.X_AXIS));
        saveLabel = new JLabel("Saving");
        saveLabel.setHorizontalAlignment(SwingConstants.LEFT);
        savePanel.add(saveLabel);
        savePanel.setVisible(false);
        //
        menuitem.addActionListener(new ActionListener(){//This action listener is called when a user wants to add a new catalogue
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    savePanel.setVisible(true);
                    final Timer saveNametimer = new Timer(500,new ActionListener(){
                        @Override
                        public void actionPerformed(ActionEvent e){
                            saveLabel.setText("Saved");
                        }
                    });
                    saveNametimer.start();
                    catClient.saveCatalogues();
                    saveNametimer.setRepeats(false);
                    Timer timer = new Timer(1000,new ActionListener(){
                        @Override
                        public void actionPerformed(ActionEvent e){
                            savePanel.setVisible(false);
                            saveLabel.setText("Saving");
                        }
                    });
                    timer.start();
                    timer.setRepeats(false);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(CatalogueGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(CatalogueGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        file.add(menuitem);
        //Third Menu Item
        menuitem = new JMenuItem("Quit");
        menuitem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(QueryDialog("Are you sure you want to quit?","Quit")){
                    pane.dispose();
                    System.exit(0);
                }
            }
        });
        file.add(menuitem);
        pane.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                if(QueryDialog("Are you sure you want to quit?","Quit")){
                    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
                else{
                    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                }
            }
        });
    }
    
    /**
     * This class sets the layout of the main panel when the user first opens 
     * the program or goes to the home page of the program.
     *
     */
    class HomePanel{
        // component variables
        /**
         * This button is used to bring out the panel for adding a new catalogue.
         */
        private JButton startCat;
        /**
         * This button is used to bring out the panel for opening an already existing catalogue.
         */
        private JButton openCat;
        
        /**
         * This is the constructor for the HomePanel class.
         */
        public HomePanel(){
            mainpanel.removeAll();
            //
            
            startCat = new JButton("Start New Catalogue");
            startCat.setForeground(Color.getHSBColor(bH, bS, bL));
            startCat.setFont(new Font("Century Gothic",Font.BOLD, 14));
            startCat.setBorder(new RoundedBorder(10));
            startCat.addActionListener(new NewCatListener());
            //
            openCat = new JButton("Open Existing Catalogue");
            openCat.setBorder(new RoundedBorder(10));
            openCat.setForeground(Color.getHSBColor(bH, bS, bL));
            openCat.setFont(new Font("Century Gothic",Font.BOLD, 14));
            openCat.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    new EditCatPanel();
                }
            });
            //
            GroupLayout layout = new GroupLayout(mainpanel);
            mainpanel.setLayout(layout);
            
            layout.setHorizontalGroup(layout.createSequentialGroup()
                    .addContainerGap(102, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(label, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(startCat,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(10)
                            .addComponent(openCat, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(250, 250, 250));
            layout.setVerticalGroup(layout.createSequentialGroup()
                    .addContainerGap(100, Short.MAX_VALUE)
                    .addComponent(label, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                    .addGap(30)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(startCat)
                        .addComponent(openCat))
                    .addContainerGap(300, Short.MAX_VALUE));

            mainpanel.validate();
            mainpanel.repaint();
            
        }
    }
    /**
     * This class creates a GUI that is used to create a new Catalogue.
     */
    class AddCatPanel {
        /**
         * This label sets out the question for the dialog box.
         */
        private JLabel addlabel = new JLabel("Enter the name of the catalogue you want to create..");
        /**
         * THis textfield is used to get the name of the new catalogue.
         */
        private JTextField nameText = new JTextField();
        /**
         * This button is used to add a new catalogue after all validations are done.
         */
        private JButton addButton = new JButton("ADD");
        private JButton homeButton = new JButton("Home");
        /**
         * This is the default constructor of the AddCatPanel class.
         */
        public AddCatPanel() {
            mainpanel.removeAll();
            GroupLayout layout = new GroupLayout(mainpanel);
            mainpanel.setLayout(layout);
            layout.setHorizontalGroup(layout.createSequentialGroup()
                    .addContainerGap(102, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(addlabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                    .addComponent(nameText, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                    .addComponent(addButton)
                    .addGap(10)
                    .addComponent(homeButton)))
                    .addGap(250, 250, 250));
            layout.setVerticalGroup(layout.createSequentialGroup()
                    .addContainerGap(200, Short.MAX_VALUE)
                    .addComponent(addlabel, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(nameText, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(addButton)
                    .addComponent(homeButton))
                    .addContainerGap(300, Short.MAX_VALUE));
            //Design for button
            addButton.setForeground(Color.getHSBColor(bH, bS, bL));
            addButton.setBorder(new RoundedBorder(3));
            addButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            homeButton.setForeground(Color.getHSBColor(bH, bS, bL));
            homeButton.setBorder(new RoundedBorder(3));
            homeButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            //
            
            
            addButton.addActionListener(new AddCatListener());
            
            homeButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    new HomePanel();
                }
            });
            mainpanel.validate();
            mainpanel.repaint();
        }

        /**
         * This class implements a listener for when the add new catalogue
         * button is pressed.
         */
        class AddCatListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (catClient.CheckString(nameText.getText())) {
                    if (!catClient.isCatalogue(nameText.getText())) {
                        catClient.AddCatalogue(nameText.getText());
                        JOptionPane.showMessageDialog(mainpanel, "The Catalogue has been successfully added", "Added Catalogue",JOptionPane.INFORMATION_MESSAGE);
                        int dialogButton = JOptionPane.YES_NO_OPTION;
                        int dialogResult = JOptionPane.showConfirmDialog(null, "Do you want to add entries to this Catalogue?", "Add", dialogButton);
                        if (dialogResult == JOptionPane.YES_OPTION) {
                            new EditCatPanel();
                        }
                        else{
                            nameText.setText("");
                        }
                    } else {
                        JOptionPane.showMessageDialog(mainpanel, "Another catalogue exists with the name you have entered", "Input Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(mainpanel, "Please enter a name", "Input Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        }
    }

    /**
     * This class creates a GUI that is used to edit a Catalogue.
     */
    class EditCatPanel {
        /**
         * This label is the main label which shows the user what to do in the current panel.
         */
        JLabel addlabel = new JLabel("Choose the catalogue you want to open and click on the open button.");
        /**
         * This label is displayed if there aren't any entries in a catalogue.
         */
        JLabel emptyListLabel = new JLabel("empty");
        /**
         * This label is used to show the catalogue that the user is viewing.
         */
        JLabel workingCatLabel = new JLabel();
        /**
         * This button is used to open a catalogue after the user has selected a catalogue's name.
         */
        JButton findButton = new JButton("Open");
        /**
         * This button is used to back to the home page.
         */
        JButton homeButton = new JButton("Home");
        /**
         * This table shows the entries of a particular catalogue.
         */
        JTable entryTable;
        /**
         * This table show the currently existing catalogues that were previously saved.
         */
        JTable searchTable;
        /**
         * This button is used to open a panel that is used to edit a catalogue.
         */
        JButton editCatButton = new JButton("Edit Catalogue Name");
        /**
         * This button is used to delete a catalogue that the user is currently working on.
         */
        JButton delCatButton = new JButton("Delete Catalogue");
        /**
         * This button is used to add a new entry to a catalogue.
         */
        JButton addCatEntryButton = new JButton("Add Catalogue Entry");
        /**
         * This button is used to edit the entries of a particular catalogue.
         */
        JButton editCatEntryButton = new JButton("Edit Catalogue Entry");
        /**
         * This button is used to view a catalogue entry.
         */
        JButton viewCatEntryButton = new JButton("View Catalogue Entry");
        /**
         * This button is used to delete a catalogue entry.
         */
        JButton delCatEntryButton = new JButton("Delete Catalogue Entry");
        /**
         * The string stores the name of the catalogue that is being currently used by the user.
         */
        String workingCatName;
        /**
         * The string holds the name of the header for the table that holds the names of the catalogue.
         */
        String[] searchColumnName = {"Catalogue Name"};
        /**
         * The string holds the name of the headers for the table that holds the names of a catalogue's entries.
         */
        String[] columnNames = {"Entry Name","Entry ID"};
        /**
         * This sets out the model of the catalogue entries table.
         */
        DefaultTableModel tableModel;
        /**
         * This sets out the model of the catalogue table.
         */
        DefaultTableModel searchTableModel;
        /**
         * This is the scroll pane for the catalogue entry table.
         */
        JScrollPane tableScroll;
        /**
         * THis is the scroll pane of the search catalogue table.
         */
        JScrollPane searchtableScroll;
        
        /**
         * This is the default constructor of the AddCatPanel class.
         */
        public EditCatPanel() {
            //button font colour
            findButton.setForeground(Color.getHSBColor(bH, bS, bL));
            homeButton.setForeground(Color.getHSBColor(bH, bS, bL));
            editCatButton.setForeground(Color.getHSBColor(bH, bS, bL));
            delCatButton.setForeground(Color.getHSBColor(bH, bS, bL));
            addCatEntryButton.setForeground(Color.getHSBColor(bH, bS, bL));
            editCatEntryButton.setForeground(Color.getHSBColor(bH, bS, bL));
            viewCatEntryButton.setForeground(Color.getHSBColor(bH, bS, bL));
            delCatEntryButton.setForeground(Color.getHSBColor(bH, bS, bL));
            //Rounded buttons
            findButton.setBorder(new RoundedBorder(5));
            homeButton.setBorder(new RoundedBorder(5));
            editCatButton.setBorder(new RoundedBorder(10));
            delCatButton.setBorder(new RoundedBorder(10));
            addCatEntryButton.setBorder(new RoundedBorder(10));
            editCatEntryButton.setBorder(new RoundedBorder(10));
            viewCatEntryButton.setBorder(new RoundedBorder(10));
            delCatEntryButton.setBorder(new RoundedBorder(10));
            //Font for buutons
            findButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            homeButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            editCatButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            delCatButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            addCatEntryButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            editCatEntryButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            viewCatEntryButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            delCatEntryButton.setFont(new Font("Century Gothic",Font.BOLD, 14));        
            //
            mainpanel.removeAll();
            GroupLayout layout = new GroupLayout(mainpanel);
            mainpanel.setLayout(layout);
            mainpanel.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentShown(ComponentEvent e) {
                    UpdateMultList();
                }
            });
            tableModel = new DefaultTableModel(null,columnNames){
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            entryTable = new JTable();
            entryTable.getTableHeader().setBackground(Color.lightGray);
            entryTable.getTableHeader().setForeground(Color.getHSBColor(bH, bS, bL));
            entryTable.getTableHeader().setFont(new Font("Century Gothic", Font.BOLD, 14));
            entryTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            entryTable.setModel(tableModel);
            tableScroll = new JScrollPane(entryTable);
            //
            searchTableModel = new DefaultTableModel(catClient.GetCatNames(),searchColumnName){
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            searchTable = new JTable();
            searchTable.getTableHeader().setBackground(Color.lightGray);
            searchTable.getTableHeader().setForeground(Color.getHSBColor(bH, bS, bL));
            searchTable.getTableHeader().setFont(new Font("Century Gothic", Font.BOLD, 14));
            searchTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            searchTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        JTable target = (JTable) e.getSource();
                        if (!searchtableScroll.isVisible()) {
                            findButton.setText("Open");
                            UpdateMultList();
                            addlabel.setVisible(true);
                            searchtableScroll.setVisible(true);
                            tableScroll.setVisible(false);
                            emptyListLabel.setVisible(false);
                            workingCatLabel.setVisible(false);
                            editCatButton.setVisible(false);
                            delCatButton.setVisible(false);
                            addCatEntryButton.setVisible(false);
                            editCatEntryButton.setVisible(false);
                            viewCatEntryButton.setVisible(false);
                            delCatEntryButton.setVisible(false);
                        } else {
                            if (target.getSelectedRow() == -1) {
                                JOptionPane.showMessageDialog(mainpanel, "Please select a catalogue to open!!", "Input Error", JOptionPane.ERROR_MESSAGE);
                            } else {
                                addlabel.setVisible(false);
                                findButton.setText("Open Another Catalogue");
                                searchtableScroll.setVisible(false);
                                workingCatName = target.getModel().getValueAt(target.getSelectedRow(), 0).toString();
                                workingCatLabel.setText("YOU ARE CURRENTLY VIEWING THE CATALOGUE: " + workingCatName);
                                workingCatLabel.setVisible(true);
                                editCatButton.setVisible(true);
                                delCatButton.setVisible(true);
                                addCatEntryButton.setVisible(true);
                                UpdateMultList();
                            }
                        }
                    }
                }
            });
            searchTable.setModel(searchTableModel);
            searchtableScroll = new JScrollPane(searchTable);
            //set the font of the labels.
            workingCatLabel.setFont(new Font("Century Gothic",Font.BOLD, 14));
            emptyListLabel.setFont(new Font("Century Gothic",Font.BOLD, 14));
            
            //set component visibility values to false
            tableScroll.setVisible(false);
            emptyListLabel.setVisible(false);
            float eH,eS,eL;//0.00, 0.82, 0.49
            eH = new Float(0.00); eS = new Float(0.82); eL = new Float(0.49);
            emptyListLabel.setForeground(Color.getHSBColor(eH, eS, eL));
            workingCatLabel.setVisible(false);
            float lH,lS,lL;
            lH = new Float(0.72); lS = new Float(0.38); lL = new Float(0.36);
            workingCatLabel.setForeground(Color.getHSBColor(lH, lS, lL));
            editCatButton.setVisible(false);
            delCatButton.setVisible(false); 
            addCatEntryButton.setVisible(false);
            editCatEntryButton.setVisible(false);
            viewCatEntryButton.setVisible(false);
            delCatEntryButton.setVisible(false);
            
            //Action Listeners for the various buttons
            //find button listener
            findButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(!searchtableScroll.isVisible()){
                        findButton.setText("Open");
                        UpdateMultList();
                        addlabel.setVisible(true);
                        searchtableScroll.setVisible(true);
                        tableScroll.setVisible(false);
                        emptyListLabel.setVisible(false);
                        workingCatLabel.setVisible(false);
                        editCatButton.setVisible(false);
                        delCatButton.setVisible(false);
                        addCatEntryButton.setVisible(false);
                        editCatEntryButton.setVisible(false);
                        viewCatEntryButton.setVisible(false);
                        delCatEntryButton.setVisible(false);
                    }
                    else{
                        if (searchTable.getSelectedRow() == -1) {
                            JOptionPane.showMessageDialog(mainpanel, "Please select a catalogue to open!!", "Input Error", JOptionPane.ERROR_MESSAGE);
                        }
                        else{
                            addlabel.setVisible(false);
                            findButton.setText("Open Another Catalogue");
                            searchtableScroll.setVisible(false);
                            workingCatName = searchTable.getModel().getValueAt(searchTable.getSelectedRow(), 0).toString();
                            workingCatLabel.setText("YOU ARE CURRENTLY VIEWING THE CATALOGUE: "+workingCatName);
                            workingCatLabel.setVisible(true);
                            editCatButton.setVisible(true);
                            delCatButton.setVisible(true); 
                            addCatEntryButton.setVisible(true);
                            UpdateMultList();
                        }
                    }
                }
            });
            //homeButton Listener
            homeButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    new HomePanel();
                }
            });
            //editCatButton listener
            editCatButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                        String[] options = {"Ok","Cancel"};
                        JLabel lbl = new JLabel("Enter the new name of the catalogue: ");
                        JTextField txt = new JTextField(10);
                        JPanel newNamePanel = new JPanel();
                        newNamePanel.add(lbl);
                        newNamePanel.add(txt);
                        int selectedOption = -1;
                        do{
                            selectedOption = JOptionPane.showOptionDialog(mainpanel, newNamePanel, "New Catalogue Name", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                            String newName = txt.getText();
                            if (selectedOption == 0) {
                                try {
                                    if (newName == null) {
                                        newName = "";
                                    }
                                    if (catClient.CheckString(newName)) {
                                        if (!catClient.isCatalogue(newName) || workingCatName.equalsIgnoreCase(newName)) {
                                            if (QueryDialog("Are you sure you want to edit the name of the catalogue", "Edit Name")) {
                                                catClient.GetCatalogueList().get(catClient.GetCatIndex(workingCatName)).SetName(newName);
                                                workingCatName = newName;
                                                workingCatLabel.setText("YOU ARE CURRENTLY VIEWING THIS CATALOGUE: " + workingCatName);
                                                JOptionPane.showMessageDialog(mainpanel, "The name of the catalogue has been changed.", "Name Change",1);
                                                selectedOption = 1;
                                            }
                                        } else {
                                            throw new Exception("Another catalogue exists with the same name");
                                        }
                                    } else {
                                        throw new Exception("Please enter a name");
                                    }
                                } catch (Exception addE) {
                                    JOptionPane.showMessageDialog(mainpanel, addE.getMessage(), "Input Error", JOptionPane.ERROR_MESSAGE);
                                }
                            }
                        }while(selectedOption == 0);
                }
            });
            //delCatButton listener
            delCatButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(QueryDialog("Are you sure you want to delete the current catalogue you are viewing?","Delete Catalogue")){
                        if(catClient.RemoveCatalogue(workingCatName)){
                            JOptionPane.showMessageDialog(mainpanel, "The catalogue has been deleted.","Delete Catalogue", 1);
                            new EditCatPanel();
                        }
                    }
                }
            });
            //addCatEntryButton listener
            addCatEntryButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e) {
                    pane.setVisible(false);
                    mainpanel.setVisible(false);
                    AddEditMult addEdit = new AddEditMult(workingCatName);
                    addEdit.AddOption();
                    addEdit.run();
                    UpdateMultList();
                }
            });
            //editCatEntryButton listener
            editCatEntryButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(entryTable.getSelectedRow() == -1){
                        JOptionPane.showMessageDialog(mainpanel, "Please select a catalogue entry!!", "Input Error", JOptionPane.ERROR_MESSAGE);
                    }
                    else{
                        pane.setVisible(false);
                        mainpanel.setVisible(false);
                        AddEditMult addEdit = new AddEditMult(workingCatName);
                        addEdit.EditOption(Integer.parseInt(entryTable.getModel().getValueAt(entryTable.getSelectedRow(), 1).toString()));
                        addEdit.run();
                        UpdateMultList();
                    }
                }
            });
            //viewCatEntryButton listener
            viewCatEntryButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e) {
                    ViewEntry view;
                    if(entryTable.getSelectedRow() == -1){
                        JOptionPane.showMessageDialog(mainpanel, "Please select a catalogue entry!!", "Input Error", JOptionPane.ERROR_MESSAGE);
                    }
                    else{
                        try {
                            view = new ViewEntry(Integer.parseInt(entryTable.getModel().getValueAt(entryTable.getSelectedRow(), 1).toString()),workingCatName);
                            view.run();
                            pane.setVisible(false);
                        } catch (IOException ex) {
                            Logger.getLogger(CatalogueGUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
            //delCatEntryButton listener
            delCatEntryButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(entryTable.getSelectedRow() == -1){
                        JOptionPane.showMessageDialog(mainpanel, "Please select a catalogue entry!!", "Input Error", JOptionPane.ERROR_MESSAGE);
                    }
                    else{
                        if (QueryDialog("Are you sure you want to delete the selected catalogue entry?", "Delete Catalogue Entry")) {
                            if (catClient.deleteMultimedia(catClient.GetCatIndex(workingCatName), Integer.parseInt(entryTable.getModel().getValueAt(entryTable.getSelectedRow(), 1).toString()))) {
                                JOptionPane.showMessageDialog(mainpanel, "The catalogue entry has been deleted.", "Delete Catalogue Entry", 1);
                                UpdateMultList();
                            }
                        }
                    }
                    
                }
            });
            //
            layout.setHorizontalGroup(layout.createSequentialGroup()
                    .addContainerGap(120, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(addlabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(searchtableScroll)
                        .addComponent(findButton)
                        .addGap(10)
                        .addComponent(homeButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(workingCatLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(editCatButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10)
                        .addComponent(delCatButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10)
                        .addComponent(addCatEntryButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(emptyListLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tableScroll)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(viewCatEntryButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10)
                        .addComponent(editCatEntryButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10)
                        .addComponent(delCatEntryButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(150, 150, 150));
            layout.setVerticalGroup(layout.createSequentialGroup()
                    .addContainerGap(30, Short.MAX_VALUE)
                    .addComponent(addlabel, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(searchtableScroll)
                        .addComponent(findButton)
                        .addComponent(homeButton))
                    .addGap(30)
                    .addComponent(workingCatLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(30)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(editCatButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(delCatButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addCatEntryButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(30)
                    .addComponent(emptyListLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tableScroll)
                    .addGap(50)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(viewCatEntryButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(editCatEntryButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(delCatEntryButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap(300, Short.MAX_VALUE));

            mainpanel.validate();
            mainpanel.repaint();
        }
        /**
         * This function is used to update the list of the entries in a 
         * catalogue and also the list of catalogues.
         */
        private void UpdateMultList(){
            if (catClient.GetCatalogueList().get(catClient.GetCatIndex(workingCatName)).GetCatalogue().isEmpty()) {
                emptyListLabel.setText("THERE ARE NO ENTRIES TO VIEW FOR THE CURRENT CATALOGUE.");
                emptyListLabel.setVisible(true);
                tableScroll.setVisible(false);
                editCatEntryButton.setVisible(false);
                viewCatEntryButton.setVisible(false);
                delCatEntryButton.setVisible(false);
            } else {
                tableModel.setDataVector(catClient.GetCatEntryNames(workingCatName), columnNames);
                tableModel.fireTableDataChanged();
                tableScroll.setVisible(true);
                emptyListLabel.setText("SELECT AN ENTRY AND THEN CLICK ONE OF THE BUTTONS BELOW.");
                editCatEntryButton.setVisible(true);
                viewCatEntryButton.setVisible(true);
                delCatEntryButton.setVisible(true);
            }
            searchTableModel.setDataVector(catClient.GetCatNames(), searchColumnName);
            searchTableModel.fireTableDataChanged();
        }
    }
    
    class AddEditMult{
        /**
         * Frame of the window for the AddEditMult class.
         */
        private JFrame addEditFrame;
        /**
         * Text field for the name of the multimedia.
         */
        private JTextField multName;
        /**
         * Text field of the ID of the text field.
         */
        private JTextField multID;
        /**
         * Text field of the text file selected for the multimedia.
         */
        private JTextField multText_textField;
        /**
         * Text field of the image file selected for the multimedia.
         */
        private JTextField multImage_textField;
        /**
         * File chooser for the text file for the multimedia.
         */
        private JFileChooser multText;
        /**
         * File chooser for the image file for the multimedia.
         */
        private JFileChooser multImage;
        /**
         * Button for adding the multimedia.
         */
        private JButton addMultButton;
        /**
         * Button for editing the multimedia.
         */
        private JButton editMultButton;
        /**
         * Button for closing the frame for the class.
         */
        private JButton cancelMultButton;
        /**
         * Button for bringing out the JFileChooser for the text file for the multimedia.
         */
        private JButton multTextButton;
        /**
         * Button for bringing out the JFileChooser for the image file for the multimedia.
         */
        private JButton multImageButton;
        /**
         * panel for the adding and editing window.
         */
        private JPanel addEditPanel;
        /**
         * Main label in the window.
         */
        private JLabel addEditLabel;
        /**
         * Label for the name of the multimedia.
         */
        private JLabel multNameLabel;
        /**
         * Label for the ID of the multimedia.
         */
        private JLabel multIDLabel;
        /**
         * Label for the text of the multimedia.
         */
        private JLabel multTextLabel;
        /**
         * Label for the image of the multimedia.
         */
        private JLabel multImageLabel;
        /**
         * Scroll Pane for the window.
         */
        private JScrollPane scroll;
        /**
         * Path of the text file chosen by the user.
         */
        private String textPath;
        /**
         * Path of the image file chosen by the user.
         */
        private String imagePath;
        /**
         * Name of catalogue being used by user.
         */
        public String addEditworkingCatName;
        /**
         * ID of the multimedia being used by the user.
         */
        private int workingMultID;
                
        /**
         * This is the default constructor of the AddEditMult class
         */
        public AddEditMult(String workingCatName){
            addEditFrame = new JFrame("Add New Multimedia");
            addEditworkingCatName = workingCatName;
            //
            multName = new JTextField();
            multNameLabel = new JLabel("Name");
            multNameLabel.setFont(new Font("Century Gothic",Font.ITALIC, 18));
            multNameLabel.setForeground(Color.getHSBColor(bH, bS, bL));
            //
            multID = new JTextField();
            multID.setEditable(false);
            multID.setText(Integer.toString(catClient.catMultimediaGetId(addEditworkingCatName)));
            multIDLabel = new JLabel("ID");
            multIDLabel.setFont(new Font("Century Gothic",Font.ITALIC, 18));
            multIDLabel.setForeground(Color.getHSBColor(bH, bS, bL));
            //
            multText = new JFileChooser();
            multText.setApproveButtonText("Select");
            multText.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter txtFilter = new FileNameExtensionFilter("txt files (*.txt)", "txt");
            multText.setVisible(false);
            multText.setFileFilter(txtFilter);
            
            multTextLabel = new JLabel("Text File");
            multTextLabel.setFont(new Font("Century Gothic",Font.ITALIC, 18));
            multTextLabel.setForeground(Color.getHSBColor(bH, bS, bL));
            multText_textField = new JTextField();
            multText_textField.setEditable(false);
            multTextButton = new JButton("Choose File");
            multTextButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    multText.setVisible(true);
                }
            });
            multText.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JFileChooser chooser = (JFileChooser) e.getSource();
                    if (JFileChooser.APPROVE_SELECTION.equals(e.getActionCommand())) {
                        try {
                            if (multText.getSelectedFile() == null) {
                                throw new Exception("Please select a text file for the multimedia.");
                            }
                            else{
                                if(catClient.isTextFile(multText.getSelectedFile().getName().toString())){
                                    multText_textField.setText(multText.getSelectedFile().getName().toString());
                                    //textPath = catClient.CopyFile(multText.getSelectedFile().getAbsolutePath().toString(), 1);
                                    multText.setVisible(false);
                                }
                                else{
                                    throw new Exception("Please choose a text with the extension (.txt)");
                                }
                            }
                        } catch (Exception f) {
                            JOptionPane.showMessageDialog(mainpanel, f.getMessage(), "Input Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else if (JFileChooser.CANCEL_SELECTION.equals(e.getActionCommand())) {
                        multText.setVisible(false);
                    }
                }
            });
            //
            multImage = new JFileChooser();
            multImage.setApproveButtonText("Select");
            multImage.setAcceptAllFileFilterUsed(false);
            multImage.setVisible(false);
            FileNameExtensionFilter imageFilter = new FileNameExtensionFilter("Images", "jpg", "jpeg", "JPG", "JPEG");
            multImage.setFileFilter(imageFilter);
            multImage.setFileSelectionMode(JFileChooser.FILES_ONLY);
            multImage_textField = new JTextField();
            multImage_textField.setEditable(false);
            multImageButton = new JButton("Choose File");
            multImageButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    multImage.setVisible(true);
                }
            });
            multImage.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JFileChooser chooser = (JFileChooser) e.getSource();
                    if (JFileChooser.APPROVE_SELECTION.equals(e.getActionCommand())) {
                        try {
                            if (multImage.getSelectedFile() == null) {
                                throw new Exception("Please select a text file for the multimedia.");
                            }
                            else{
                                if(catClient.isImageFile(multImage.getSelectedFile().getName().toString())){
                                    multImage_textField.setText(multImage.getSelectedFile().getName().toString());
                                    //imagePath = catClient.CopyFile(multImage.getSelectedFile().getAbsolutePath().toString(), 2);
                                    multImage.setVisible(false);
                                }else{
                                    throw new Exception("Please choose an image with the extension (.jpg) or (.jpeg)");
                                }
                            }
                        } catch (Exception f) {
                            JOptionPane.showMessageDialog(mainpanel, f.getMessage(), "Input Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else if (JFileChooser.CANCEL_SELECTION.equals(e.getActionCommand())) {
                        multImage.setVisible(false);
                    }
                }
            });
            multImageLabel = new JLabel("Image File");
            multImageLabel.setFont(new Font("Century Gothic",Font.ITALIC, 18));
            multImageLabel.setForeground(Color.getHSBColor(bH, bS, bL));
            //
            addMultButton = new JButton("Add Multimedia");
            editMultButton = new JButton("Edit Multimedia");
            cancelMultButton = new JButton("Cancel");
            cancelMultButton.setVisible(true);
            //
            addEditPanel = new JPanel();
            addEditPanel.setBackground(Color.getHSBColor(H, S, L));
            addEditLabel = new JLabel();
            addEditPanel.setAutoscrolls(true);
            //
            GroupLayout multLayout = new GroupLayout(addEditPanel);
            addEditPanel.setLayout(multLayout);
            scroll = new JScrollPane(addEditPanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED ,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            addEditFrame.add(scroll);
            
            //button Font
            addMultButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            editMultButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            cancelMultButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            multTextButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            multImageButton.setFont(new Font("Century Gothic",Font.BOLD, 14));        
            //button Colour
            addMultButton.setForeground(Color.getHSBColor(bH, bS, bL));
            editMultButton.setForeground(Color.getHSBColor(bH, bS, bL));
            cancelMultButton.setForeground(Color.getHSBColor(bH, bS, bL));
            multTextButton.setForeground(Color.getHSBColor(bH, bS, bL));
            multImageButton.setForeground(Color.getHSBColor(bH, bS, bL));       
            //Rounded Buttons
            addMultButton.setBorder(new RoundedBorder(10));
            editMultButton.setBorder(new RoundedBorder(10));
            cancelMultButton.setBorder(new RoundedBorder(10));
            multTextButton.setBorder(new RoundedBorder(10));
            multImageButton.setBorder(new RoundedBorder(10));
            //
            multLayout.setHorizontalGroup(multLayout.createSequentialGroup()
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(addEditLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(multNameLabel,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(multName, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(multIDLabel,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(multID, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(multTextLabel,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(multText_textField, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                            .addComponent(multTextButton))
                        .addComponent(multText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(multImageLabel,GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(multImage_textField, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                            .addComponent(multImageButton))
                        .addComponent(multImage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(addMultButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(20)
                            .addComponent(editMultButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(20)
                            .addComponent(cancelMultButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(20, 20, 20));
            multLayout.setVerticalGroup(multLayout.createSequentialGroup()
                    .addComponent(addEditLabel, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(multNameLabel, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(multName, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE))
                    .addGap(20)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(multIDLabel, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(multID, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE))
                    .addGap(20)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(multTextLabel, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(multText_textField, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(multTextButton))
                    .addGap(20)
                    .addComponent(multText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(multImageLabel, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(multImage_textField, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(multImageButton))
                    .addGap(20)
                    .addComponent(multImage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(GroupLayout.PREFERRED_SIZE, 120,  Short.MAX_VALUE)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(addMultButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(editMultButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cancelMultButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(20));
                    
            //
            addEditFrame.addWindowListener(new WindowAdapter(){
                 @Override
                 public void windowClosing(WindowEvent we)
                 {
                    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    mainpanel.setVisible(true);
                    pane.setVisible(true);
                 }
            });
            //
            addMultButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    try{
                        if (!catClient.CheckString(multName.getText())) {
                            throw new Exception("Please enter a name for the multimedia.");
                        }

                        if (!catClient.CheckString(multText_textField.getText())) {
                            throw new Exception("Please select a text file for the multimedia.");
                        }

                        if (!catClient.CheckString(multImage_textField.getText())) {
                            throw new Exception("Please select an image file for the multimedia.");
                        }
                        textPath = catClient.CopyFile(multText.getSelectedFile().getAbsolutePath().toString(), 1);
                        imagePath = catClient.CopyFile(multImage.getSelectedFile().getAbsolutePath().toString(), 2);
                        if(!catClient.addMultimedia(catClient.GetCatIndex(addEditworkingCatName), multName.getText(), Integer.parseInt(multID.getText()), catClient.GetCreationDate(), textPath, imagePath)){
                            throw new Exception("The Multimedia cannot be added at this moment.");
                        }
                        else{
                            JOptionPane.showMessageDialog(addEditPanel, "The multimedia has been added to the catalogue.", "Added Multimedia", JOptionPane.INFORMATION_MESSAGE);
                            addEditFrame.dispose();
                            mainpanel.setVisible(true);
                            pane.setVisible(true);
                            
                        }
                    }catch(Exception f){
                        JOptionPane.showMessageDialog(mainpanel, f.getMessage(), "Input Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
            //
            editMultButton.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    try{
                        if (!catClient.CheckString(multName.getText())) {
                            throw new Exception("Please enter a name for the multimedia.");
                        }

                        if (!catClient.CheckString(multText_textField.getText())) {
                            throw new Exception("Please select a text file for the multimedia.");
                        }

                        if (!catClient.CheckString(multImage_textField.getText())) {
                            throw new Exception("Please select an image file for the multimedia.");
                        }
                        
                        
                        if(textPath == null){
                            textPath = "";
                        }
                        else{
                            textPath = catClient.CopyFile(multText.getSelectedFile().getAbsolutePath().toString(), 1);
                        }
                        if(imagePath == null){
                            imagePath = "";
                        }
                        else{
                            imagePath = catClient.CopyFile(multImage.getSelectedFile().getAbsolutePath().toString(), 2);
                        }
                        if(!catClient.updateMultInfo(catClient.GetCatIndex(addEditworkingCatName), workingMultID,multName.getText(), textPath, imagePath)){
                            throw new Exception("The Multimedia cannot be edited at this moment.");
                        }
                        else{
                            JOptionPane.showMessageDialog(addEditPanel, "The multimedia has been updated.", "Updated Multimedia", JOptionPane.INFORMATION_MESSAGE);
                            addEditFrame.dispose();
                            mainpanel.setVisible(true);
                            pane.setVisible(true);
                            
                        }
                   }catch(Exception f){
                       JOptionPane.showMessageDialog(mainpanel, f.getMessage(), "Input Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
            //
            cancelMultButton.addActionListener(new ActionListener(){
                 @Override
                 public void actionPerformed(ActionEvent e){
                     addEditFrame.dispose();
                     mainpanel.setVisible(true);
                     pane.setVisible(true);
                 }
            });
        }
        /**
         * Method to run the window in adding a multimedia format.
         * The window will have the add button instead of the edit button.
         */
        public void AddOption(){
            addMultButton.setVisible(true);
            editMultButton.setVisible(false);
        }
        /**
         *  Method to run the window in editing a multimedia format.
         * The window will have the edit button instead of the add button.
         */
        public void EditOption(int multIDNum){
            workingMultID = multIDNum;
            multID.setVisible(false);
            multIDLabel.setVisible(false);
            Multimedia workingMult = catClient.GetCatalogueList().get(catClient.GetCatIndex(addEditworkingCatName)).GetCatalogue().get(catClient.GetMultIndex(catClient.GetCatIndex(addEditworkingCatName), multIDNum));
            multName.setText(workingMult.GetName());
            multText_textField.setText(new File(workingMult.GetText()).getName());
            multImage_textField.setText(new File(workingMult.GetImage()).getName());
            addMultButton.setVisible(false);
            editMultButton.setVisible(true);
        }
        /**
         * Method to show the window.
         */
        public void run(){
            addEditFrame.setSize(900, 500);
            addEditFrame.setLocationRelativeTo(pane);
            addEditFrame.setVisible(true);
        }
    }
    
    class ViewEntry{
        /**
         * Frame for the Class.
         */
        private JFrame viewFrame;
        /**
         * Text field for the name of the multimedia.
         */
        private JTextField multName;
        /**
         * Text field for the ID of the multimedia.
         */
        private JTextField multID;
        /**
         * Text field for the Date of Creation of the multimedia.
         */
        private JTextField multDOC;
        /**
         * Text area for the text file name of the multimedia.
         */
        private JTextArea multText_textArea;
        /**
         * Button for the cancel button which closes the window.
         */
        private JButton cancelMultButton;
        /**
         * Panel for the window.
         */
        private JPanel viewPanel;
        /**
         * Main label of the window.
         */
        private JLabel addEditLabel;
        /**
         * Label for the name of the multimedia.
         */
        private JLabel multNameLabel;
        /**
         * Label for the ID of the multimedia.
         */
        private JLabel multIDLabel;
        /**
         * Label for the Date of Creation of the multimedia. 
         */
        private JLabel multDOCLabel;
        /**
         * Label for the text file name of the multimedia.
         */
        private JLabel multTextLabel;
        /**
         * Label for the image file of the multimedia.
         */
        private JLabel multImageLabel;
        /**
         * Label for the image of the multimedia.It shows the image of the multimedia.
         */
        private JLabel imageLabel;
        /**
         * 
         */
        private JScrollPane scroll;
        /**
         * Scroll pane of the JTextarea.
         */
        private JScrollPane textareaScroll;
        /**
         * Name of the catalogue being used by the user.
         */
        private String workingCatName;
        /**
         * ID of the multimedia being used by the user.
         */
        private int entryID;
        
        /**
         * This is the main constructor of the ViewEntry class.
         * @throws IOException 
         */
        public ViewEntry(int _multID,String _workingCatName) throws IOException{
            workingCatName = _workingCatName;
            entryID = _multID;
            Multimedia workingmult = catClient.GetCatalogueList().get(catClient.GetCatIndex(workingCatName)).GetCatalogue().get(catClient.GetMultIndex(catClient.GetCatIndex(workingCatName), entryID));
            viewFrame = new JFrame("View Multimedia");
            //
            multName = new JTextField(workingmult.GetName());
            multName.setEditable(false);
            multNameLabel = new JLabel("Name");
            multNameLabel.setFont(new Font("Century Gothic",Font.ITALIC, 18));
            multNameLabel.setForeground(Color.getHSBColor(bH, bS, bL));
            //
            multID = new JTextField(workingmult.GetId());
            multID.setEditable(false);
            multID.setText(Integer.toString(workingmult.GetId()));
            multIDLabel = new JLabel("ID");
            multIDLabel.setFont(new Font("Century Gothic",Font.ITALIC, 18));
            multIDLabel.setForeground(Color.getHSBColor(bH, bS, bL));
            //
            multDOC = new JTextField(workingmult.GetDate().toString());
            multDOC.setEditable(false);
            multDOCLabel = new JLabel("Date Of Creation");
            multDOCLabel.setFont(new Font("Century Gothic",Font.ITALIC, 18));
            multDOCLabel.setForeground(Color.getHSBColor(bH, bS, bL));
            //
            multTextLabel = new JLabel("Text");
            multTextLabel.setFont(new Font("Century Gothic",Font.ITALIC, 18));
            multTextLabel.setForeground(Color.getHSBColor(bH, bS, bL));
            if(!catClient.CheckMultFile(workingmult.GetText())){
                multText_textArea = new JTextArea("No text to display either because file is corrupt or empty");
                textareaScroll = new JScrollPane(multText_textArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            }
            else
            {
                
                multText_textArea = catClient.GetMultTextFromFile(workingmult.GetText());
                textareaScroll = new JScrollPane(multText_textArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            }
            textareaScroll.setPreferredSize(new Dimension(100,100));
            multText_textArea.setEditable(false);
            //
            imageLabel = new JLabel("Image");
            imageLabel.setFont(new Font("Century Gothic",Font.ITALIC, 18));
            imageLabel.setForeground(Color.getHSBColor(bH, bS, bL));
            if(!catClient.CheckMultFile(workingmult.GetText())){
                multImageLabel = new JLabel("No image to display either because file is corrupt or empty");
            }
            else
            {
                multImageLabel = new JLabel();
                multImageLabel.setSize(300, 250);
                multImageLabel.setIcon(catClient.GetMultImage(multImageLabel,workingmult.GetImage()));
            }
            //
            cancelMultButton = new JButton("Close");
            cancelMultButton.setFont(new Font("Century Gothic",Font.BOLD, 14));
            cancelMultButton.setForeground(Color.getHSBColor(bH, bS, bL));
            cancelMultButton.setBorder(new RoundedBorder(10));
            cancelMultButton.setVisible(true);
            //
            viewPanel = new JPanel();
            addEditLabel = new JLabel();
            //
            GroupLayout multLayout = new GroupLayout(viewPanel);
            viewPanel.setLayout(multLayout);
            viewPanel.setBackground(Color.getHSBColor(H, S, L));
            scroll = new JScrollPane(viewPanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            viewFrame.getContentPane().add(viewPanel);
            //
            multLayout.setHorizontalGroup(multLayout.createSequentialGroup()
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGap(20)
                        .addComponent(addEditLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(multNameLabel,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(multName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(multIDLabel,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(multID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(multDOCLabel,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(multDOC, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(multTextLabel,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(textareaScroll))
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(imageLabel,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(20)
                            .addComponent(multImageLabel,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(multLayout.createSequentialGroup()
                            .addGap(20)
                            .addComponent(cancelMultButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(20, 20, 20));
            multLayout.setVerticalGroup(multLayout.createSequentialGroup()
                    .addContainerGap(30, Short.MAX_VALUE)
                    .addComponent(addEditLabel, GroupLayout.PREFERRED_SIZE, 30, Short.MAX_VALUE)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(multNameLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(multName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(20)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(multIDLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(multID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(20)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(multDOCLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(multDOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(20)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(multTextLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(textareaScroll,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(20)
                    .addGroup(multLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(imageLabel,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(multImageLabel,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    )
                    .addGap(40)
                    .addComponent(cancelMultButton,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap(10, Short.MAX_VALUE)
                    );
                    
            //
            viewFrame.addWindowListener(new WindowAdapter(){
                 @Override
                 public void windowClosing(WindowEvent we)
                 {
                    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    pane.setVisible(true);
                 }
            });
            //
            cancelMultButton.addActionListener(new ActionListener(){
                 @Override
                 public void actionPerformed(ActionEvent e){
                     viewFrame.dispose();
                     pane.setVisible(true);
                 }
            });
        }
        public void setEntryId(int _id){
            entryID = _id;
        }
        /**
         * 
         */
        public void run(){
            viewFrame.setSize(900, 650);
            viewFrame.setLocationRelativeTo(pane);
            viewFrame.setVisible(true);
        }
    }

    /**
     * This class implements a listener for the button when the start new
     * catalogue menu option is pressed.
     */
    class NewCatListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            new AddCatPanel();
        }
    }
    /**
     * This method pops up a dialog with a question to the user and 
     * the ye and no options as buttons.
     * @param query question to the user.
     * @param title title of the dialog box.
     * @return boolean.
     */
    private boolean QueryDialog(String query,String title){
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(mainpanel, query, title, dialogButton);
        if (dialogResult == JOptionPane.YES_OPTION) {
            return true;
        }
        return false;
    }
    /**
     * This class is used to create rounded borders for components.
     */
    class RoundedBorder implements Border {
        
        /**
         * the radius of each corner of the component.
         */
        private int radius;
        
        /**
         * Constructor of the class.
         * @param radius the radius of the corners of the component.
         */
        public RoundedBorder(int radius) {
            this.radius = radius;
        }
        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(this.radius+1, this.radius+1, this.radius+2, this.radius);
        }


        @Override
        public boolean isBorderOpaque() {
            return true;
        }


        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.drawRoundRect(x,y,width-1,height-1,radius,radius);
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        CatalogueGUI gui = new CatalogueGUI("Catalogues");
        
        gui.setSize(900, 700);
        gui.setLocationRelativeTo(null);
        gui.setVisible(true);
    }
}
