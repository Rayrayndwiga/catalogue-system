The AC Cobra was a British sports car that was designed and built in the 1960s. 
American auto racer Carroll Shelby wrote a letter AC Cars and requested a car modified to accept an V8 engine, a move that set the way for the AC Shelby Cobra. 
Eventually, the Shelby Cobra would become a successful car in many racing circuits, even though it was not meant to race.